#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Класс сервера """
import socket
import threading as t
import logging as lg


class Server:
    """ Класс сервера
     Interface:
       .Server(port)
       .start()
       .shutdown()
       .connected()
       .scrape_new_messages()
       .scrape_new_connected()
    """

    def __init__(self, port):
        """
        Инициализирует сервер.
            Начинается автоматическая прослушка порта,
            всех подключившихся пользователей
        """
        self.port = port
        self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server.bind(('', port))
        self._server.listen(5)
        self._server.settimeout(1)
        self.hostip = socket.gethostbyname(socket.gethostname())

        self._connected = {}
        self.__mutex = t.Semaphore(1)

        self._new_ip = []

        self._new_messages = []
        self.__messmutex = t.Semaphore(1)
        self.__can_recv = True

        self.__listening_thread = t.Thread(target=self.__listen)
        self.__can_listen = True

    def start(self):
        """ Запускает процесс прослушивания порта. """
        self.__listening_thread.start()

    def shutdown(self):
        """ Выключает сервер. """
        self.__can_listen = False
        self.__can_recv = False

    def __del__(self):
        self.shutdown()
        self._server.close()

    def __listen(self):
        """
            Процесс прослушивания порта.
            Запускает __client_thread при новых подключениях.
        """
        lg.info("listen port: " + str(self.port))
        while self.__can_listen:
            try:
                client_socket = self._server.accept()[0]
            except socket.timeout:
                continue

            # Запускаем поток для полученного клиента
            clnt = t.Thread(target=self.__client_thread, args=(client_socket,))
            clnt.start()
        return

    def kick(self, ip, port):
        """ Кик. """
        with self.__mutex:
            if (ip, port) in self._connected:
                self._connected.pop((ip, port)).close()
            if (ip, port) in self._new_ip:
                self._new_ip.remove((ip, port))

    def __client_thread(self, client):
        """ Поток для клиента.

            1) Принимает контактные данные от подключенного клиента.
            2) Отпрвляет список своих контактов для подключения.
            3) Начинает принимать и анализировать сообщения от пользователя.
        """
        # знакомимся

        ip = client.getpeername()[0]

        port = 0
        try:
            port = int(client.recv(1024).decode())
        except (ConnectionResetError, ConnectionAbortedError):
            lg.warning('Connection error!')
            return
        else:
            with self.__mutex:
                if (ip, port) not in self._connected:
                    self._connected[(ip, port)] = client
                    self._new_ip += [(ip, port)]
                    lg.info('client Connectd: ' + str((ip, port)))
                else:
                    lg.warning(str((ip, port)) + ' already in contact list.')
                    return

        try:
            conn = str(list(self.connected()))
            if conn:
                client.send(conn.encode())
                lg.info('Send my contacts to ' + str((ip, port)))
        except (ConnectionResetError, ConnectionAbortedError):
            lg.warning('Contacts sending failed.')

        # завершили знакомство

        client.settimeout(1)

        while (ip, port) in self._connected and self.__can_recv:
            try:
                message = client.recv(1024).decode()
                if not message:
                    raise BrokenPipeError

                if len(message) > 0:
                    lg.debug('> {}: {}'.format((ip, port), message))
                    with self.__messmutex:
                        message = '{}|{}'.format((ip, port), message)
                        self._new_messages += [message]

            except (ConnectionResetError,
                    ConnectionAbortedError,
                    BrokenPipeError):
                self.kick(ip, port)
                lg.info('client Disconnected: ' + str((ip, port)))
            except socket.timeout:
                continue

    def connected(self):
        """ Вернет массив всех подключенных. """
        return self._connected.keys()

    def scrape_new_messages(self):
        """ Выгрести все отправленные на сервер сообщения. """
        res = self._new_messages
        with self.__messmutex:
            self._new_messages = []
        return res

    def scrape_new_connected(self):
        """ Выгрести все новые ip. """
        res = self._new_ip
        with self.__mutex:
            self._new_ip = []
        return res


if __name__ == '__main__':
    pass
