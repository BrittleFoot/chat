import unittest
import time
import re
from TClient import Client
from TServer import Server


class TestUsercommands(unittest.TestCase):

    def test_commands(self):
        from TUserCommands import UserCommandsInvoker
        from PlushFakes import PlushChat

        invoker = UserCommandsInvoker(PlushChat())
        invoker('sdsds')
        invoker('/help')
        invoker('/help who')
        invoker('/ban')
        invoker('/ban smb')
        invoker('/ban 127.0.0.1')
        invoker('/ban -r smb')
        invoker('/ban -r -a')
        invoker('/chname Petka')
        invoker('/clear')
        invoker('/connect 127.0.0.1 1232')
        invoker('/exit')
        invoker('/w 127.0.0.1 3455 smth')
        invoker('/w name smth')
        invoker('/sound on')
        invoker('/sound off')
        invoker('/who')


class TestClientToServerFunctionality(unittest.TestCase):

    def test_client_and_server(self):
        client = Client(65155)
        server = Server(65155)
        server.start()

        client.connect_one('127.0.0.1', 65155)

        time.sleep(1)
        assert(client.connected() == server.connected())

        server.shutdown()
        client.stop()

    def test_one_server_twenty_clients(self):
        server = Server(65156)
        server.start()
        clients = []
        for x in range(20):
            client = Client(65156 + x)
            clients.append(client)

        for client in clients:
            client.connect_one('127.0.0.1', 65156)

        time.sleep(5)
        assert(len(server.connected()) == len(clients))
        for client in clients:
            client.stop()
        server.shutdown()

    def test_util_getip(self):
        from util.getip import get_local_addr
        get_local_addr('127.0.0.1')
        ip_founded = get_local_addr(ipv6=False)
        if re.match(r'\d+\.\d+.\d+\.\d+', ip_founded):
            assert(1)
        else:
            assert(0)

    def test_util_namegen(self):
        from util.namegenerator import NameGenerator
        generator = NameGenerator()
        name = generator()
        assert(name)

    def test_util_orctransl(self):
        from util.orcsinterpr import OrcTranslator
        translator = OrcTranslator()
        phrase = translator('Some Test Phrase!')
        assert(phrase)
        assert(translator('lol').find('Kek') > -1)
        assert(translator('лол').find('Kek') > -1)


if __name__ == '__main__':
    unittest.main()
