#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Здесь есть класс обработки комманд пользователя. """
import re
import threading as t
import time


class ArgumentError(Exception):
    """ Класс для генерации ArgumentError """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'ArgumentError: ' + self.message


class UserCommandsInvoker(object):
    """ Описывает функции для комманд пользователя """
    def __init__(self, chat):
        self._chat = chat
        self._commands = {}
        self.base_registrate()

    def base_registrate(self):
        """ Регистрирует комманды. """
        self.reg('sendall', self.sendall)
        self.reg('connect', self.connect)
        self.reg('chname', self.chname)
        self.reg('who', self.who)
        self.reg('exit', self.exit)
        self.reg('help', self.help)
        self.reg('sound', self.sound)
        self.reg('w', self.whisper)
        self.reg('ban', self.ban)
        self.reg('clear', self.clear)

    def reg(self, keyword, function):
        """ Записывает комманду в словарь. """
        self._commands[keyword] = function

    def __call__(self, command):
        self.recognize_and_invoke(command)

    def recognize_and_invoke(self, command):
        """ Собственно главная задача класса. """
        try:
            keyword, args = self.recognize(command)
            self.invoke(keyword.lower(), args)
        except ArgumentError as e:
            self._chat._print(str(e))
        except KeyError:
            self._chat._print('Command not found. Type /help.')

    def recognize(self, command):
        """ Распознать """
        try:
            if command[0] == '/':
                splited = command.split(maxsplit=1)
                if len(splited) == 1:
                    return splited[0][1:], None
                keyword, args = splited
                keyword = keyword[1:]
                args = args.split()
                return keyword, args
            else:
                return 'sendall', command
        except (TypeError, ValueError):
            raise ArgumentError('Invalid command arguments')

    def invoke(self, keyword, args):
        """ И вызвать """
        try:
            return self._commands[keyword](args)
        except (TypeError, ValueError):
            raise ArgumentError('Invalid command arguments')

    """ Далее идут процедуры вызываемые позьзователем.
    Докстринга метода обязательно нужна для метода /help <function>
    """

    def sendall(self, mess):
        """ Send messae to anyone.
        Automaticly invoke for any non-command message.
        """
        partionized = []
        messcopy = mess

        while messcopy:
            partionized += [messcopy[:300]]
            messcopy = messcopy[300:]

        for m in partionized:
            message = '{}: {}'.format(self._chat.name, m)
            self._chat.client.sendall(message)
            if len(partionized) > 1:
                time.sleep(0.1)

    def connect(self, ip_port):
        """ Connect command.
        Usage: /connect <ip> <port>
        """
        ip, port = ip_port
        if len(port) > 5:
            raise ArgumentError('port is number between 0-65500')
        port = int(port)

        if not re.match(r'\d+\.\d+.\d+\.\d+', ip):
            raise ArgumentError('ip should be is in xx.xxx.xxx.xxx format')

        self._chat.client.connect_one(ip, port)
        self.say_hello()

    def say_hello(self):
        say = t.Thread(target=self.__say_hello)
        say.start()

    def __say_hello(self):
        """ Вспомогательная функция для connect """
        time.sleep(0.3)
        message = '{}: {}'.format(self._chat.name, '')
        self._chat.client.sendall(message)

    def chname(self, new_name):
        """ Change name.
        Usage: /chname <new_name>
               /chname -r  <- random name
        """
        if new_name[0] == '-r':
            new_name = self._chat.generate_name().split()
        last_name = self._chat.name
        new_name = '_'.join(new_name)
        self._chat.name = new_name

        message = 'User "{}" now is "{}"'.format(last_name, new_name)
        message = '{} chname: {}'.format(self._chat.name, message)
        self._chat.client.sendall(message)

    def who(self, cmd):
        """
        Print list of connected names & ip's & port's.
        """
        conn = self._chat.name_by_addres
        conn = ['{} has addres {}'.format(n, a) for a, n in conn.items()]
        conn = sorted(conn, key=lambda s: s.split()[0])
        self._chat._print('>>> List of connected:\n' +
                          '   >>> ' + '\n   >>> '.join(conn))

    def exit(self, nan):
        """
        Shut down the server.
        """
        nan = str(nan)
        self._chat.shutdown()

    def help(self, cmd):
        """ Help funtion.
        Usage: /help [function]
        """
        if cmd:
            cmd = cmd[0]
            if cmd not in self._commands:
                raise ArgumentError('Command unknown: ' + cmd)
            self._chat._print('>>> Help of funtion "{}":'.format(cmd))
            self._chat._print(self._commands[cmd].__doc__)
            return

        self._chat._print('>>> Available functions:')
        self._chat._print('>>> Type /help <function> for more information.')
        fnklist = sorted(self._commands.keys(), key=lambda s: s)
        fnklist = '   >>> ' + '\n   >>> '.join(fnklist)
        self._chat._print(fnklist)

    def sound(self, cmd):
        """ Controlling sound support of new messages.
        Usage: /sound <on|off>
        """
        cmd = cmd[0].lower()
        if cmd == 'on':
            self._chat.sound_is_on = True
            self._chat._print('>>> Sound is ON.')
        elif cmd == 'off':
            self._chat._print('>>> Sound is OFF.')
            self._chat.sound_is_on = False
        else:
            raise ArgumentError('usage /sound on|off')

    def whisper(self, cmd):
        """ Send private message.
        Usage: /w <name>|<ip port> <message>
        """
        form = '{} whispered: {}'
        if not re.match(r'\d+\.\d+.\d+\.\d+', cmd[0]):
            name = cmd[0]
            for adr, nm in self._chat.name_by_addres.items():
                if nm == name:
                    message = ' '.join(cmd[1:])
                    message = form.format(self._chat.name, message)
                    self._chat.client.send(adr, message.encode())
        else:
            addrs = (cmd[0], int(cmd[1]))
            message = ' '.join(cmd[2:])
            message = form.format(self._chat.name, message)
            self._chat.client.send(addrs, message.encode())

    def ban(self, cmd):
        """ Operating with the banlist.
        /ban               -> list of banned.
        /ban <ip|name>     -> add to banlist
        /ban -r <ip|name>  -> delete from banlis
        /ban -r -a         -> unban all!
        /ban -o|+o         -> change orcs mode
        """
        if not cmd:
            banlist = self._chat.banlist
            banprint = ''
            if banlist:
                banprint = '   >>> ' + '\n   >>> '.join(self._chat.banlist)
            else:
                banprint = '   >>> Everybody is free!'
            self._chat._print('>>> List of banned:')
            self._chat._print(banprint)
        else:
            if cmd[0] == '-r':
                if cmd[1:]:
                    if cmd[1] == '-a':
                        self._chat.unban(unban_all=True)
                        self._chat._print('>>> Amnisty! \*-*|')
                        return
                    unbanned = []
                    for word in cmd[1:]:
                        try:
                            self._chat.unban(word)
                        except KeyError:
                            continue
                        else:
                            unbanned.append(word)
                    if not unbanned:
                        self._chat._print('>>> No one unbanned :c')
                        return
                    self._chat._print('>>> Unbanned: ' + ', '.join(unbanned))
                else:
                    raise ArgumentError('Select smb to unban.')
            elif cmd[0] == '-o':
                self._chat.i_am_ork = False
            elif cmd[0] == '+o':
                self._chat.i_am_ork = True
            else:
                for word in cmd:
                    self._chat.ban(word)
                self._chat._print('>>> Banned: ' + ', '.join(cmd))

    def clear(self, cmd):
        """ Clear screen.
        Clear all log information.
        """
        self._chat.io.clear_screen()


if __name__ == '__main__':
    cm = UserCommandsInvoker('mok')
    cm('234234')
