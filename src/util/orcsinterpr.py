#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random


class OrcTranslator(object):
    """Translate phrese into orcs lang"""
    def __init__(self):
        self.randint = random.Random().randint
        f = False
        paths = [
            'data\\Phrases.txt',
            'src\\util\\data\\Phrases.txt',
            'data/Phrases.txt',
            'src/util/data/Phrases.txt',
            'util\\data\\Phrases.txt',
            'util/data/Phrases.txt'
        ]
        for path in paths:
            try:
                f = open(path, encoding='utf-8')
            except FileNotFoundError:
                pass
            else:
                self.data = f.read().split('\n')
                f.close()
                f = True
                break

        if not f:
            raise FileNotFoundError
        else:
            self.data = [x.split(', ') for x in self.data]
            self.dictionary = dict()
            for arr in self.data:
                self.dictionary[len(arr[0])] = arr

    def __call__(self, message):
        return self.translate(message)

    def translate(self, message):
        message = message.split()
        orcs = []
        for word in message:
            orc_word = self.orcs(word)
            orcs += [orc_word + self.rsymbol(word == message[-1])]
        return ' '.join(orcs)

    def orcs(self, word):
        mysteryis_orcus_hash = lambda char: ord(char)
        from itertools import accumulate
        hashlist = list(map(mysteryis_orcus_hash, word))
        hashvar = list(accumulate(accumulate(hashlist)))[-1]
        len_word = min(len(word), 13)
        words = self.dictionary[len_word]
        if word.lower() in {'lol', 'лол'}:
            hashvar = 4
        return words[hashvar % len(words)]

    def rsymbol(self, end_of_text=False):
        symb = set('?!,.:')
        symb.add('...')
        symb.add(' -')
        if end_of_text:
            symb.remove(',')
            symb.remove(':')
            symb.remove(' -')

        luck = random.choice([0, 0, 1])
        if luck or end_of_text:
            return random.choice(list(symb))
        else:
            return ''
