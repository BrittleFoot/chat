#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random


class NameGenerator(object):
    """Generate random name"""
    def __init__(self):
        f = False
        paths = [
            'data\\names.en.txt',
            'src\\util\\data\\names.en.txt',
            'data/names.en.txt',
            'src/util/data/names.en.txt',
            'util/data/names.en.txt',
            'util\\data\\names.en.txt'
        ]
        for path in paths:
            try:
                f = open(path, encoding='utf-8')
            except FileNotFoundError:
                pass
            else:
                self.data = f.readlines()
                f.close()
                f = True
                break

        if not f:
            raise FileNotFoundError

    def __call__(self):
        name = random.choice(self.data)
        if name[-2:] == ' \n':
            name = name[:-2]
        return name
