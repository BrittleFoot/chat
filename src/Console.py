#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Класс консоли с красивым readline"""
import threading as t
import time
import sys
from os import system
try:
    from src.util.Getch import getch
except ImportError:
    from util.Getch import getch


class console(object):
    """
    Берет на себя ответственность за
    красивый и потокобезопасный readline
    """
    usertext = ''
    __mutex = t.Semaphore(1)
    __print = True
    __beginread = False
    prompt = '>>'
    is_win = sys.platform == 'win32'

    def start():
        """ Начинает поддержку readline """
        tr = t.Thread(target=console.writeinp)
        tr.start()

    def stop():
        """ останавливает захват этим классом управление консолью"""
        console.__print = False

    def change_prompt(new_prompt):
        """ Изменяет строку - приглашение для ввода
        Стандартно: '>>> '
        """
        console.prompt = new_prompt

    def play_song():
        """ проигрывает звуковой сигнал """
        print('\a', end='')

    def writeinp():
        """
        Процесс, следящий за тем,
        чтобы пользовательский ввод не пересекался с выводом на консоль.
        """
        counter = 0
        while console.__print:
            dash = counter >= 25 and console.__beginread and console.is_win
            endl = ['', '_'][dash]
            strl = ['', console.prompt + '> '][console.__beginread]
            with console.__mutex:
                outputline = '\r' + strl + console.usertext + endl
                if len(outputline) > 75:
                    outputline = outputline[0:75] + '...'
                sys.stdout.write(outputline)
                sys.stdout.flush()
            time.sleep(0.05)
            counter = (counter + 1) % 50

    def writeline(line):
        """ writeline как он есть """
        with console.__mutex:
            console.clearline()
            sys.stdout.write(str(line) + '\n')
            sys.stdout.flush()

    def readline(linesize=128, predicate=lambda ch: True):
        """ Читает строку до нажатия Enter.
        Прекращает ввод если строка больше чем linesize.
        predicate - булева функция определяющаяя - какие символы можно вводить.
        """
        console.__beginread = True
        while 1:
            ch = getch()
            if ch == b'\r' or ch == '\r':
                inp = console.usertext
                with console.__mutex:
                    console.clearline()
                    console.usertext = ''
                    console.__beginread = False
                return inp
            elif ch == b'\x08' or ch == '\x7f':
                with console.__mutex:
                    console.clearline()
                    console.usertext = console.usertext[0:-1]
            elif len(console.usertext) < linesize:
                if console.is_win:
                    char = ch.decode(sys.stdout.encoding)
                else:
                    char = ch
                if len(char) == 1 and predicate(char):
                    console.usertext += char

    def clearline():
        """ очищает строку """
        sys.stdout.write('\r' + ' ' * 79 + '\r')
        sys.stdout.flush()

    def clear_screen():
        system(['clear', 'cls'][console.is_win])


if __name__ == '__main__':
    pass
