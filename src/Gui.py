#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Класс для создания обьекта графического интерфейса.
    Обьект будет использован в качестве io_object для чата.
    io_object - обьект имеющий интерфейс:
            *.start()
            *.stop()
            *.writeline(line)
            *.readline([n_of_char], [predicate])
            *.change_prompt(new_prompt)
            *.play_song()
            *.clear_screen()
"""
import time
import threading as t
from tkinter import *
from tkinter.scrolledtext import ScrolledText
try:
    from src.util.tooltip import CreateToolTip
except ImportError:
    from util.tooltip import CreateToolTip


def hello(event=None):
    pass


class Application(Frame):

    """ Сам графический интерфейс.
    Структура:
        self
            menubar
                filemenu
                    Connect
                    Change name
                    Exit
                helpmenu
                    about
            main_frame
                clear_button
                log
            who_frame
                who_button
                conn_list
            i_frame
                chname_button
                send_button
                entry
    """

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.root = master
        self.master.minsize(550, 200)
        self.master.title('i_o chat')

        self.root.protocol('WM_DELETE_WINDOW', self.stop_i_sad)
        self.__can_be_closed = True
        self.__started = False
        self.__in_chat = False

        self.userinput = StringVar()
        self.username = StringVar()
        self.username.set('>>>')
        self.message_sended = t.Event()
        self.__message_to_send = ''

        self.__create_widgets()
        self.__connect_signals()

        self.entry.focus_set()
        self.bind_all('<Key>', self.set_focus_on_entry)

        self.__in_dialog = False
        from tkinter.messagebox import askyesno, showinfo
        from tkinter.simpledialog import askstring
        self.askyesno = self.i_dialog(askyesno)
        self.askstring = self.i_dialog(askstring)
        self.showinfo = self.i_dialog(showinfo)

        self.client_list_needs_update = False

    def set_focus_on_entry(self, e):
        if e and not self.__in_dialog:
            self.entry.focus_set()

    def i_am_in_chat(self):
        self.__in_chat = True

    def start(self):
        self.__started = True
        self.__can_be_closed = False

        self.__can_update = True
        self.__who_update = t.Thread(target=self.__upd_who)
        self.__who_update.start()

    def support(self, chat):
        self.__chat = chat

    def stop(self):
        self.__can_update = False
        self.root.quit()

    def stop_i_sad(self):
        if self.askyesno('Exit?', 'Are you sure?'):
            if not self.__in_chat:
                self.stop()
            if not self.__started:
                self.send('q')
            elif not self.__can_be_closed:
                self.send('/exit')
            else:
                self.stop()

    def i_dialog(self, dialog):
        def new_dialog_func(*args, **kwargs):
            self.__in_dialog = True
            answ = dialog(*args, **kwargs)
            self.__in_dialog = False
            return answ
        return new_dialog_func

    def can_be_closed(self):
        self.__can_be_closed = True
        self.writeline('NOW YOU CAN CLOSE WINDOW.')

    def __create_widgets(self):
        self.__create_io_zone()
        self.__create_who_zone()
        self.__create_log_zone()
        self.__create_menu()

    def __create_menu(self):
        """ Создает меню.
        """
        self.menubar = Menu(self)

        # create a pulldown menu, and add it to the menu bar
        filemenu = Menu(self.menubar, tearoff=0)
        filemenu.add_command(label="Connect", command=self.connect)
        filemenu.add_command(label="Change name", command=self.chname)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.stop_i_sad)
        self.menubar.add_cascade(label="Options", menu=filemenu)

        helpmenu = Menu(self.menubar, tearoff=0)
        helpmenu.add_command(label="About", command=self.about)
        self.menubar.add_cascade(label="Help", menu=helpmenu)

        # display the menu
        self.root.config(menu=self.menubar)

    def __create_io_zone(self):
        self.i_frame = Frame()
        self.i_frame.pack(side='bottom', fill='x', expand=False)

        self.send_button = Button(self.i_frame)
        self.send_button.config(width=15, command=hello, text='Send')
        self.send_button['bg'] = 'lightgreen'
        self.send_button.pack(side='right', fill='none', expand=False)

        self.chname_button = Button(self.i_frame)
        self.chname_button.config(width=15, textvariable=self.username)
        self.chname_ttp = CreateToolTip(self.chname_button, 'Your name.')
        self.chname_button['command'] = hello
        self.chname_button['bg'] = 'lightblue'
        self.chname_button.pack(side='left', fill='y', expand=False)

        self.entry = Entry(self.i_frame, textvariable=self.userinput)
        self.entry['bd'] = 5
        self.entry.pack(fill='x', expand=True)

    def __create_who_zone(self):
        self.who_frame = Frame()
        self.who_frame['bd'] = 1
        self.who_frame['relief'] = SUNKEN
        self.who_frame['bg'] = 'lightgreen'
        self.who_frame.pack(side='right', fill='y', expand=False)

        self.who_button = Button(
            self.who_frame, text='Who is there?', width=14)
        self.who_button['bd'] = 1
        self.who_button['bg'] = 'lightgreen'
        self.who_button['command'] = hello
        self.who_button.pack(side='top')

        # Список подключенных.
        self.conn_list = Text(self.who_frame)
        self.conn_list.config(state=DISABLED, bg='lightgreen', width=13, bd=1)
        self.conn_list.pack(fill='both', expand=1)

        def conn_list_insert(message):
            self.conn_list['state'] = NORMAL
            self.conn_list.insert(END, message)
            self.conn_list['state'] = DISABLED
            self.conn_list.see(END)

        def conn_list_clear():
            self.conn_list['state'] = NORMAL
            self.conn_list.delete('0.0', END)
            self.conn_list['state'] = DISABLED

        self.conn_list.add = conn_list_insert
        self.conn_list.clear = conn_list_clear

    def __create_log_zone(self):
        self.main_frame = Frame()
        self.main_frame['bg'] = 'lightblue'
        self.main_frame['bd'] = 1
        self.main_frame['relief'] = SUNKEN
        self.main_frame.pack(fill='both', expand=1)

        self.clear_button = Button(self.main_frame, text='Clear', bd=1)
        self.clear_button['bg'] = 'lightblue'
        self.clear_button['command'] = hello
        self.clear_button.pack(side='top', fill='x')

        self.log = ScrolledText(self.main_frame)
        self.log.config(state=DISABLED, bg='lightblue', wrap='word', bd=1)
        self.log.pack(fill='both', expand=1)

        def log_insert(message):
            self.log['state'] = NORMAL
            self.log.insert(END, message)
            self.log['state'] = DISABLED
            self.log.see(END)

        def log_clear():
            if self.askyesno('LOST MEMORY (-)_(-)', 'Are you sure?'):
                self.log['state'] = NORMAL
                self.log.delete('0.0', END)
                self.log['state'] = DISABLED

        self.log.add = log_insert
        self.log.clear = log_clear

    def __connect_signals(self):
        self.entry.bind('<Return>', self.send_from_entry)
        self.send_button['command'] = self.send_from_entry
        self.chname_button['command'] = self.chname

        self.who_button['command'] = self.update_connected

        self.clear_button['command'] = self.log.clear

    def writeline(self, line):
        if (str(line).split('\n')[0]) == '>>> List of connected:':
            if self.client_list_needs_update:
                self.client_list_needs_update = False
                self.update_client_list(line)
                return
        self.log.add(str(line) + '\n')

    def readline(self):
        self.message_sended.wait()
        self.message_sended.clear()
        message = self.__message_to_send
        self.__message_to_send = ''
        return message

    def change_prompt(self, new_prompt):
        prompt = new_prompt
        if len(prompt) > 18:
            prompt = prompt[0:15] + '...'
        self.username.set(prompt)
        self.chname_ttp.change(new_prompt)
        self.chname_button

    def play_song(self):
        self.bell()

    def clear_screen(self):
        self.log.clear()

    def send(self, message):
        self.__message_to_send = message
        self.message_sended.set()

    def send_from_entry(self, event=None):
        self.send(self.userinput.get())
        self.userinput.set('')

    def chname(self):
        mess = 'Please, input your new name or "-r" for random:'
        name = self.askstring('Passport table', mess)
        if name:
            self.send('/chname ' + name)

    def connect(self):
        qvest = 'Input <ip> <port>, which one you want to connect.'
        answ = self.askstring('Connection', qvest)
        if answ:
            self.send('/connect ' + answ)

    def __upd_who(self):
        while self.__can_update:
            time.sleep(1)
            self.update_connected()

    def update_connected(self):
        self.client_list_needs_update = True
        self.send('/who')

    def update_client_list(self, line):
        lines = line.split('\n')[1:]
        names = [x.split()[1] for x in lines]
        names_short = []
        for name in names:
            n = ''
            if len(name) > 13:
                n = name[:10] + '...'
            else:
                n = name
            names_short.append(n.center(13))
        self.conn_list.clear()
        self.conn_list.add('\n'.join(names_short))

    def about(self):
        description = [
            'Programm i_o chat.',
            'This is decentralizated chat.',
            '\t',
            'First of all you need to choise port.',
            'Next - follow to /help command \*-*| Good Luck |*-*/',
            '\t',
            '(c) Igor Ostanin at Urfu Mat-mex 2015',
            'vk.com/igor0stanin',
        ]
        self.showinfo('About', '\n'.join(description))


if __name__ == '__main__':
    root = Tk()
    app = Application(master=root)
    app.mainloop()
