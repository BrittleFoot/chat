#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Класс клиента """
import socket
import threading
import time
import logging as lg


class Client:
    """
    # Interface:
    #   .Client(myport)
    #   .connect_one(ip, port)
    #   .connect_all(ip)
    #   .kick(ip)
    #   .connected()
    #   .sendall(message)
    #   .send(ip, portm message)
    """

    def __init__(self, myport):
        """
        Инициализация,
        запуск потока подключения ко всем контактам того к кому подключается.
        """
        self.myport = myport
        self._servers = {}
        self._new_contacts = []
        self.__mutex = threading.Semaphore(1)
        self.__conn_all_mutex = threading.Semaphore(1)
        self.__can_connect_all = True
        self.con_all_thrd = threading.Thread(target=self.__connect_all)
        self.con_all_thrd.start()

    def __del__(self):
        self.__can_connect_all = False
        for sock in self._servers.values():
            sock.close()

    def stop(self):
        """ Остановка сервера """
        self.__can_connect_all = False
        for sock in self._servers.values():
            sock.close()

    def connect_one(self, ip, port):
        """
        В новом потоке подключестся на указанные IP и PORT.
        Возвращает сокет, настроенный на подключенный сервер.
        """
        conn_thrd = threading.Thread(target=self._connect_one, args=(ip, port))
        conn_thrd.start()

    def _connect_one(self, ip, port):
        """ см. connect_one """
        if (ip, port) in self._servers:
            lg.info('{} already exist in "servers"'.format((ip, port)))
            return False

        lg.info('Try connect: ' + str((ip, port)))
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.settimeout(3)
        try:
            server_socket.connect((ip, port))
        except (ConnectionRefusedError, socket.timeout, OSError) as e:
            server_socket.close()
            lg.warning('Server ' + ip + '\n' + str(e))
            return False

        lg.info('Connected: ' + str((ip, port)))
        server_socket.send(str(self.myport).encode())

        new_contacts = []
        try:
            new_contacts = eval(server_socket.recv(1024).decode())
            lg.debug('I get new contacts: ' + str(new_contacts))
            with self.__conn_all_mutex:
                self._new_contacts += new_contacts

        except socket.timeout:
            lg.warning(str((ip, port)) + ' wasn\'t sent new contacts')

        server_data = server_socket.getpeername()

        with self.__mutex:
            self._servers[server_data] = server_socket

        return server_data

    def __connect_all(self):
        """
        Бесконечный процесс подключения всех новых контактов.
        (см. self._connect_one)
        """
        while self.__can_connect_all:
            contacts = []
            with self.__conn_all_mutex:
                contacts = self._new_contacts
                self._new_contacts = []

            for ip, port in contacts:
                self.connect_one(ip, port)

            time.sleep(0.1)

    def kick(self, ip, port):
        """ Кикает из списка подключенных сервер с указанным IP """
        lg.info('kick: ' + str((ip, port)))
        with self.__mutex:
            self._servers.pop((ip, port))

    def connected(self):
        """ Возвращает массив всех подключенных IP """
        return self._servers.keys()

    def sendall(self, message):
        """ Отправляет НЕПУСТОЕ сообщение на все подключенные IP """
        if message:
            for srv in self._servers.values():
                try:
                    srv.send(message.encode())
                except (ConnectionResetError, ConnectionAbortedError):
                    lg.warning('Message sending failed')
        else:
            raise ValueError

    def send(self, addrs, bytemessage):
        """ Отправляет байты на указанный адрес. """
        if bytemessage:
            self._servers[addrs].send(bytemessage)
        else:
            raise ValueError


if __name__ == '__main__':
    pass
