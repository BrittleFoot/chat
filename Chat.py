#!/usr/bin/env python
# -*- coding: utf-8 -*-
import threading as t
from src.TServer import Server
from src.TClient import Client
from src.Console import console
from src.Gui import Application
from src.util.namegenerator import NameGenerator
from src.util.orcsinterpr import OrcTranslator
from tkinter import Tk
from src.TUserCommands import UserCommandsInvoker
from src.util.getip import get_local_addr
import argparse
import time
import logging as lg


lg.basicConfig(level=lg.INFO,
               filename='client_log.log',
               format='%(filename)s::%(funcName)s %(lineno)3d> %(message)s')


class Chat:

    def __init__(self, port, io_object):
        """
        Конструктор инициализации чата:
        Описание аргументов:
            port - целое число 0-65535
            io_object - обьект имеющий интерфейс:
                *.start()
                *.stop()
                *.writeline(line)
                *.readline()
                *.change_prompt(new_prompt)
                *.play_song()
                *.clear_screen()
        """
        self.server = Server(port)
        self.client = Client(port)
        self.io = io_object
        self._print = self.io.writeline
        self._input = self.io.readline
        self._chprmt = self.io.change_prompt

        self.cmd_executer = UserCommandsInvoker(self)
        self.sound_is_on = False

        self.__can_connback = True
        self.__can_sync = True
        self.__can_print = True

        self.conb_thrd = t.Thread(target=self.__connback)
        self.sync_thrd = t.Thread(target=self.__synchronize)
        self.mess_thrd = t.Thread(target=self.__print_new_messages)
        self.uinp_thrd = t.Thread(target=self.__user_input)

        self.translator = OrcTranslator()
        self.generate_name = NameGenerator()
        self.name = '_'.join(self.generate_name().split())
        self.adreses = [get_local_addr(ipv6=False)]
        self.name_by_addres = {}
        self.banlist = set()
        self.i_am_ork = False
        self.__ban_mutex = t.Semaphore(1)
        self.__name_mutex = t.Semaphore(1)

    def ban(self, name):
        with self.__ban_mutex:
            self.banlist.add(name)

    def unban(self, name=None, unban_all=False):
        with self.__ban_mutex:
            if unban_all:
                self.banlist.clear()
            else:
                self.banlist.remove(name)

    def start(self):
        self.io.start()
        self.server.start()
        self.conb_thrd.start()
        self.sync_thrd.start()
        self.mess_thrd.start()
        self.uinp_thrd.start()

        internal_ip = [x for x in self.adreses if x and x[0] != '2']
        if internal_ip:
            internal_ip = internal_ip[0]
        else:
            internal_ip = '127.0.0.1'

        self.client.connect_one(internal_ip, self.server.port)

        self.print_prompt()
        self.cmd_executer.say_hello()

    def print_prompt(self):
        self._print('   Your port is: ' + str(self.server.port))
        self._print('/--------------------------------------------------\\')
        self._print('|' + 'Chat is ready for chatting!'.center(50) + '|')
        s = 'Thise ip is yours:'
        self._print('|   {:47}|'.format(s))
        for addrs in self.adreses:
            self._print('|     {:45}|'.format(addrs))
        s = 'Type /help for available commands \*-*/.'
        self._print('|   {:47}|'.format(s))
        s = 'Do not forget to check contact list with /who'
        self._print('|   {:47}|'.format(s))
        self._print('\\--------------------------------------------------/')

    def shutdown(self):
        self._print('Exitting, please, wait 1-2 seconds...')
        self.__can_connback = False
        self.__can_sync = False
        self.__can_print = False
        self.client.stop()
        self.server.shutdown()
        self.io.stop()

    def __connback(self):
        while self.__can_connback:
            newconnected = self.server.scrape_new_connected()
            if newconnected:
                for ip, port in newconnected:
                    lg.info('connback: ' + str((ip, port)))
                    self.client.connect_one(ip, port)
                self.cmd_executer.say_hello()
            time.sleep(1)

    def __synchronize(self):
        while self.__can_sync:
            time.sleep(1)
            connected_to_server = self.server.connected()
            to_kick = []
            for ip, port in self.client.connected():
                if (ip, port) not in connected_to_server:
                    to_kick.append((ip, port))
            for ip, port in to_kick:
                self.client.kick(ip, port)

            disconnected = []
            for addres in self.name_by_addres.keys():
                if addres not in self.server.connected():
                    disconnected.append(addres)

            with self.__name_mutex:
                for addres in disconnected:
                    self.name_by_addres.pop(addres)

    def __print_new_messages(self):
        while self.__can_print:
            time.sleep(0.1)
            new_mess = self.server.scrape_new_messages()
            if new_mess:
                messages = []
                for message in new_mess:
                    msg = self.message_analys(message)
                    if msg:
                        messages += [msg]

                if messages:
                    self._print('\n'.join(set(messages)))
                    if self.sound_is_on:
                        self.io.play_song()

    def message_analys(self, message):
        if message:
            addres_str, msg = message.split('|', maxsplit=1)
            addres = eval(addres_str)
            splitted = msg.split(': ', 1)
            name = splitted[0].split()[0]

            sms = ''
            if len(splitted) > 1:
                sms = splitted[1]

            self.update_client_name(addres, name)
            ip = addres[0]

            with self.__ban_mutex:
                if name in self.banlist or ip in self.banlist:
                    if sms and not self.i_am_ork:
                        sms = self.translator(sms)
                    else:
                        return None

            if sms and splitted[-1]:
                return splitted[0] + ': ' + sms

    def update_client_name(self, addres, name):
        with self.__name_mutex:
            self.name_by_addres[addres] = name

    def __user_input(self):
        while self.__can_print:
            self._chprmt(self.name)
            command = self._input()
            if command:
                self.cmd_executer(command)


def let_chhatting_with_gui(io_object):
    io_object.i_am_in_chat()
    while 1:
        try:
            io_object.writeline('Please, input port or "q" to quit:')
            port = io_object.readline()
            if port == 'q':
                io_object.can_be_closed()
                io_object.stop()
                return
            port = int(port)
            if port < 1234 or port > 65535:
                raise ValueError
        except ValueError:
            io_object.writeline('Port must be an integer between 1234-65535')
        else:
            break
    chat = Chat(port, io_object)
    io_object.support(chat)
    chat.start()


def main_gui():
    root = Tk()
    io_object = Application(root)
    ct = t.Thread(target=let_chhatting_with_gui, args=(io_object,))
    ct.start()
    io_object.mainloop()


def main_console():
    console.start()

    while 1:
        try:
            console.writeline('Please, input port or "q" to quit:')
            just_numbers = lambda ch: ch in set('1234567890q')
            port = console.readline(predicate=just_numbers)
            if port == 'q':
                console.stop()
                return
            port = int(port)
            if port < 1234 or port > 65535:
                raise ValueError
        except ValueError:
            console.writeline('Port must be an integer between 1234-65535')
        else:
            break
    chat = Chat(port, console)
    chat.start()

if __name__ == '__main__':
    description = """
    Programm i_o chat.
        This is decentralizated chat.

        First of all you need to choise port.
        Next - follow to /help command \*-*| Good Luck |*-*/

        (c) Igor Ostanin at Urfu Mat-mex 2015
                vk.com/igor0stanin
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=description)

    cnslhelp = 'Starting program in a console mode.'
    parser.add_argument('-c', '--console',
                        dest='chatting', action='store_const',
                        const=main_console, default=main_gui,
                        help=cnslhelp)
    args = parser.parse_args()
    args.chatting()
